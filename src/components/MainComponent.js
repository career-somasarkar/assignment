import React,{Components} from 'react';
import Home from './HomeComponent';
import { Switch, Route, Redirect } from 'react-router-dom';
import Menu from './MenuComponent';

class Main extends Components{
  constructor(props){
    super(props);
    this.state = {dishes: this.props.dishes}
  }
  render() {
    const HomePage = () => {
      return(
          <Home />
      );
    }
return(
    <Switch>
        <Route path='/home' component={HomePage} />
        <Route exact path='/menu' component={() => <Menu dishes={this.state.dishes} />} />
        <Redirect to="/home" />
    </Switch>
);
}
  }

  export default Main;