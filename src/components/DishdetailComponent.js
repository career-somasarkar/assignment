import React from 'react';
import { Card, CardImg, CardText, CardBody,
    CardTitle } from 'reactstrap';

    function RenderDish({dish}){
       return(<div className="col-sm-6 col-md-6 col-lg-6">
            <CardImg top src={dish.image} alt={dish.name} />
                <CardBody>
                    <CardTitle>{dish.name}</CardTitle>
                    <CardText>{dish.description}</CardText>
                </CardBody>
        </div>);
    }

    function RenderComments({dish}){
        var comments = dish.comments.map(comm=>{
            return (
                <div key={comm.id}>
                    <div>
                        {comm.comment}                        
                    </div>
                    <div>
                        --{comm.author} , {new Intl.DateTimeFormat('en-US', { year: 'numeric', month: 'short', day: '2-digit'}).format(new Date(Date.parse(comm.date)))} 
                    </div><br/>
                </div>
            );
        });
        return(<div className="col-sm-6 col-md-6 col-lg-6">
            <CardBody>
                <CardTitle>
                    <h4>Comments</h4>
                <div>
                    {comments}
                </div>
                </CardTitle>
                <CardText>{dish.description}</CardText>
            </CardBody>
        </div>);
    }

    const DishDetail = (props) => {
        console.log(props.dish);          
        if (props.dish != null)
        {            
            return(
                <div>
                    <Card>
                        <div className="row">
                           <RenderDish dish={props.dish} />
                           <RenderComments dish= {props.dish} />
                        </div>
                    </Card>
                </div>
            );
        }
        else
            return(
                <div>no data</div>
            );
    }


export default DishDetail;