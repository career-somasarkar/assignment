
import React from 'react';
import { Card, CardImg, CardImgOverlay,
    CardTitle } from 'reactstrap';
 //import DishDetail from './DishdetailComponent';

    function RenderMenuItem ({dish, onClick}) {
        return (
            <Card onClick={() => this.onClick(dish.id)}>
                <CardImg width="10%" src={dish.image} alt={dish.name} />
                <CardImgOverlay>
                    <CardTitle>{dish.name}</CardTitle>
                </CardImgOverlay>                
            </Card>
        );
    }

    const Menu = (props) => {
        const menu = props.dishes.map((dish) => {
            return (
                <div className="col-12 col-md-5 m-1"  key={dish.id}>
                    <RenderMenuItem dish={dish} onClick={props.onClick} />
                     {/* <DishDetail dish={dish}/>  */}
                </div>
            );
        });

        return (
            <div className="container">
                <div className="row">
                    {menu}
                </div>
                <div className="row">
                  <div  className="col-12 col-md-5 m-1">
                    {/* {this.renderDish(this.state.selectedDish)} */}
                    
                  </div>
                </div>
            </div>
        );
    }
export default Menu;